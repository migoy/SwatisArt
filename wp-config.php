<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'C:\wamp\www\wordpress\wp-content\plugins\wp-super-cache/' );
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A/Do m=zxr$te=qz`TY_pq8 ~yPPJ# q:[5&O.M~w;#GJc`MvwAMk-4PBtgYau@u');
define('SECURE_AUTH_KEY',  'U/#6+K)3;=Y=! !:lyWa:1 Le#a@f `5*AutGjBc7.>7a|XD#{1gNlny9wzOJbqE');
define('LOGGED_IN_KEY',    'orvvo)Y#W<?A]Oz7uv_rP0a+wN4=ttI)eQ9_NhY@Oz{2bDJPM6i6%I+ZN`BA$#{[');
define('NONCE_KEY',        '. D3.Q2S7q#yLc5sP9d;*-KhrR0)4T@^qKe!V3ay$%;W1rW|rK6}v=0VvG%_ofmR');
define('AUTH_SALT',        'xCO N5~*,ZUwUdpb+KTP)[M(CU)`Wc`wg@XiwKRIe3;i]d7riPrY*;__xY`q598r');
define('SECURE_AUTH_SALT', '7;vCMH3h&y416]!KxK?4L0&;Fb=P&JT0jS^VhM2dAP8[Q~6nM 8HN#4-5*5iMCZP');
define('LOGGED_IN_SALT',   '5~Z`yc)NuNxesZ:Lc#<vm;$a$^lIw*0$Kzkeb`k{ fT8?+Bm2PS RlY?4`v(:y8G');
define('NONCE_SALT',       '?iO}6[o{L0j4J6;pY[Z|IGys|c[5 VCk6*,3h,WYgX,/B1JBxX$~p5dU)Ol!i;)c');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


