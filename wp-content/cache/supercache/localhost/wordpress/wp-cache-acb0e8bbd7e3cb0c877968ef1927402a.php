<?php die(); ?><!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="http://localhost/wordpress/xmlrpc.php">

				<script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
			<title>Festivals</title>
			<style>
				.wishlist_table .add_to_cart, a.add_to_wishlist.button.alt { border-radius: 16px; -moz-border-radius: 16px; -webkit-border-radius: 16px; }			</style>
		<link rel='dns-prefetch' href='//apis.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title=" &raquo; Feed" href="http://localhost/wordpress/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title=" &raquo; Comments Feed" href="http://localhost/wordpress/?feed=comments-rss2" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/localhost\/wordpress\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.5"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='gtranslate-style-css'  href='http://localhost/wordpress/wp-content/plugins/gtranslate/gtranslate-style24.css?ver=4.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='sydney-wc-css-css'  href='http://localhost/wordpress/wp-content/themes/sydney/woocommerce/css/wc.css?ver=1.45' type='text/css' media='all' />
<link rel='stylesheet' id='sydney-bootstrap-css'  href='http://localhost/wordpress/wp-content/themes/sydney/css/bootstrap/bootstrap.min.css?ver=1.45' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://localhost/wordpress/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='mig-main-style-css'  href='http://localhost/wordpress/wp-content/plugins/migoy/css/style.css?ver=4.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='sow-social-media-buttons-atom-cae6c2a67eae-css'  href='http://localhost/wordpress/wp-content/uploads/siteorigin-widgets/sow-social-media-buttons-atom-cae6c2a67eae.css?ver=4.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce-layout-css'  href='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/css/woocommerce-layout.css?ver=3.3.5' type='text/css' media='all' />
<style id='woocommerce-layout-inline-css' type='text/css'>

	.infinite-scroll .woocommerce-pagination {
		display: none;
	}
</style>
<link rel='stylesheet' id='woocommerce-smallscreen-css'  href='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css?ver=3.3.5' type='text/css' media='only screen and (max-width: 768px)' />
<link rel='stylesheet' id='aws-style-css'  href='http://localhost/wordpress/wp-content/plugins/advanced-woo-search/assets/css/common.css?ver=1.39' type='text/css' media='all' />
<link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'  href='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/css/prettyPhoto.css?ver=3.3.5' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-selectBox-css'  href='http://localhost/wordpress/wp-content/plugins/yith-woocommerce-wishlist/assets/css/jquery.selectBox.css?ver=1.2.0' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-main-css'  href='http://localhost/wordpress/wp-content/plugins/yith-woocommerce-wishlist/assets/css/style.css?ver=2.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='yith-wcwl-font-awesome-css'  href='http://localhost/wordpress/wp-content/plugins/yith-woocommerce-wishlist/assets/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
<link rel='stylesheet' id='sydney-fonts-css'  href='https://fonts.googleapis.com/css?family=Lobster+Two%7CLobster+Two' type='text/css' media='all' />
<link rel='stylesheet' id='sydney-style-css'  href='http://localhost/wordpress/wp-content/themes/sydney/style.css?ver=1.45' type='text/css' media='all' />
<style id='sydney-style-inline-css' type='text/css'>
body, #mainnav ul ul a { font-family:'Lobster Two', cursive!important;}
h1, h2, h3, h4, h5, h6, #mainnav ul li a, .portfolio-info, .roll-testimonials .name, .roll-team .team-content .name, .roll-team .team-item .team-pop .name, .roll-tabs .menu-tab li a, .roll-testimonials .name, .roll-project .project-filter li a, .roll-button, .roll-counter .name-count, .roll-counter .numb-count button, input[type="button"], input[type="reset"], input[type="submit"] { font-family:'Lobster Two', cursive;}
.site-title { font-size:55px; }
.site-description { font-size:66px; }
#mainnav ul li a { font-size:30px; }
h1 { font-size:30px; }
h2 { font-size:30px; }
h3 { font-size:32px; }
h4 { font-size:25px; }
h5 { font-size:20px; }
h6 { font-size:18px; }
body { font-size:24px; }
.single .hentry .title-post { font-size:40px; }
.header-image { background-size:cover;}
.header-image { height:300px; }
.widget-area .widget_fp_social a,#mainnav ul li a:hover, .sydney_contact_info_widget span, .roll-team .team-content .name,.roll-team .team-item .team-pop .team-social li:hover a,.roll-infomation li.address:before,.roll-infomation li.phone:before,.roll-infomation li.email:before,.roll-testimonials .name,.roll-button.border,.roll-button:hover,.roll-icon-list .icon i,.roll-icon-list .content h3 a:hover,.roll-icon-box.white .content h3 a,.roll-icon-box .icon i,.roll-icon-box .content h3 a:hover,.switcher-container .switcher-icon a:focus,.go-top:hover,.hentry .meta-post a:hover,#mainnav > ul > li > a.active, #mainnav > ul > li > a:hover, button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, .text-color, .social-menu-widget a, .social-menu-widget a:hover, .archive .team-social li a, a, h1 a, h2 a, h3 a, h4 a, h5 a, h6 a,.classic-alt .meta-post a,.single .hentry .meta-post a { color:#a9d800}
.reply,.woocommerce div.product .woocommerce-tabs ul.tabs li.active,.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.project-filter li a.active, .project-filter li a:hover,.preloader .pre-bounce1, .preloader .pre-bounce2,.roll-team .team-item .team-pop,.roll-progress .progress-animate,.roll-socials li a:hover,.roll-project .project-item .project-pop,.roll-project .project-filter li.active,.roll-project .project-filter li:hover,.roll-button.light:hover,.roll-button.border:hover,.roll-button,.roll-icon-box.white .icon,.owl-theme .owl-controls .owl-page.active span,.owl-theme .owl-controls.clickable .owl-page:hover span,.go-top,.bottom .socials li:hover a,.sidebar .widget:before,.blog-pagination ul li.active,.blog-pagination ul li:hover a,.content-area .hentry:after,.text-slider .maintitle:after,.error-wrap #search-submit:hover,#mainnav .sub-menu li:hover > a,#mainnav ul li ul:after, button, input[type="button"], input[type="reset"], input[type="submit"], .panel-grid-cell .widget-title:after { background-color:#a9d800}
.roll-socials li a:hover,.roll-socials li a,.roll-button.light:hover,.roll-button.border,.roll-button,.roll-icon-list .icon,.roll-icon-box .icon,.owl-theme .owl-controls .owl-page span,.comment .comment-detail,.widget-tags .tag-list a:hover,.blog-pagination ul li,.hentry blockquote,.error-wrap #search-submit:hover,textarea:focus,input[type="text"]:focus,input[type="password"]:focus,input[type="datetime"]:focus,input[type="datetime-local"]:focus,input[type="date"]:focus,input[type="month"]:focus,input[type="time"]:focus,input[type="week"]:focus,input[type="number"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="color"]:focus, button, input[type="button"], input[type="reset"], input[type="submit"], .archive .team-social li a { border-color:#a9d800}
.site-header.float-header { background-color:rgba(172,224,40,0.9);}
@media only screen and (max-width: 1024px) { .site-header { background-color:#ace028;}}
.site-title a, .site-title a:hover { color:#dd0000}
.site-description { color:#ffffff}
#mainnav ul li a, #mainnav ul li::before { color:#ffffff}
#mainnav .sub-menu li a { color:#ffffff}
#mainnav .sub-menu li a { background:#1c1c1c}
.text-slider .maintitle, .text-slider .subtitle { color:#ffffff}
body { color:#dd1a1a}
#secondary { background-color:#ffffff}
#secondary, #secondary a, #secondary .widget-title { color:#767676}
.footer-widgets { background-color:#252525}
.btn-menu { color:#ffffff}
#mainnav ul li a:hover { color:#dd0000}
.site-footer { background-color:#1c1c1c}
.site-footer,.site-footer a { color:#666666}
.overlay { background-color:#000000}
.page-wrap { padding-top:183px;}
.page-wrap { padding-bottom:100px;}
@media only screen and (max-width: 780px) { 
    	h1 { font-size: 32px;}
		h2 { font-size: 28px;}
		h3 { font-size: 22px;}
		h4 { font-size: 18px;}
		h5 { font-size: 16px;}
		h6 { font-size: 14px;}
    }

</style>
<link rel='stylesheet' id='sydney-font-awesome-css'  href='http://localhost/wordpress/wp-content/themes/sydney/fonts/font-awesome.min.css?ver=1.45' type='text/css' media='all' />
<!--[if lte IE 9]>
<link rel='stylesheet' id='sydney-ie9-css'  href='http://localhost/wordpress/wp-content/themes/sydney/css/ie9.css?ver=1.45' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='popup-maker-site-css'  href='//localhost/wordpress/wp-content/uploads/pum/pum-site-styles.css?generated=1520827228&#038;ver=1.7.15' type='text/css' media='all' />
<link rel='stylesheet' id='sccss_style-css'  href='http://localhost/wordpress?sccss=1&#038;ver=4.9.5' type='text/css' media='all' />
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/migoy/js/main.js?ver=4.9.5'></script>
<script type='text/javascript' src='https://apis.google.com/js/platform.js?ver=4.9.5'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/wp-image-zoooom/assets/js/jquery.image_zoom.min.js?ver=1.25'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var IZ = {"with_woocommerce":"0","exchange_thumbnails":"0","woo_categories":"0","enable_mobile":"1","options":{"lensShape":"square","lensSize":200,"lensBorderSize":2,"lensBorderColour":"#ffffff","borderRadius":0,"cursor":"zoom-in","zoomWindowWidth":400,"zoomWindowHeight":360,"zoomWindowOffsetx":10,"borderSize":1,"borderColour":"#888888","zoomWindowShadow":4,"lensFadeIn":0.5,"lensFadeOut":0.5,"zoomWindowFadeIn":0.5,"zoomWindowFadeOut":0.5,"easingAmount":20,"tint":"true","tintColour":"#ffffff","tintOpacity":0.5},"woo_slider":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/wp-image-zoooom/assets/js/image_zoom-init.js?ver=1.25'></script>
<link rel='https://api.w.org/' href='http://localhost/wordpress/index.php?rest_route=/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/wordpress/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/wordpress/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.5" />
<meta name="generator" content="WooCommerce 3.3.5" />
<link rel="canonical" href="http://localhost/wordpress/?page_id=1099" />
<link rel='shortlink' href='http://localhost/wordpress/?p=1099' />
<link rel="alternate" type="application/json+oembed" href="http://localhost/wordpress/index.php?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Flocalhost%2Fwordpress%2F%3Fpage_id%3D1099" />
<link rel="alternate" type="text/xml+oembed" href="http://localhost/wordpress/index.php?rest_route=%2Foembed%2F1.0%2Fembed&#038;url=http%3A%2F%2Flocalhost%2Fwordpress%2F%3Fpage_id%3D1099&#038;format=xml" />
	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
		<style type="text/css">
		.header-image {
			background-image: url(http://localhost/wordpress/wp-content/uploads/2018/03/30a3231a84e472a561ba69480a5f5073406c39d8-2.jpg);
			display: block;
		}
		@media only screen and (max-width: 1024px) {
			.header-inner {
				display: block;
			}
			.header-image {
				background-image: none;
				height: auto !important;
			}		
		}
	</style>
	<link rel="icon" href="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-IMG-20171004-WA0024-32x32.jpg" sizes="32x32" />
<link rel="icon" href="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-IMG-20171004-WA0024-192x192.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-IMG-20171004-WA0024-180x180.jpg" />
<meta name="msapplication-TileImage" content="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-IMG-20171004-WA0024-270x270.jpg" />
		<style type="text/css" id="wp-custom-css">
			.roll-button{
	top: 150px;
}	
.single-product div.product .woocommerce-product-gallery.woocommerce-product-gallery--columns-5.flex-control-thumbs li {
    width: 50.1%;
		position:relative;

		
}
 
.single-product div.product .woocommerce-product-gallery.woocommerce-product-gallery--columns-5.flex-control-thumbs li:nth-child(5n) {
    margin-left: 0;
}		</style>
	</head>

<body class="page-template-default page page-id-1099">

	<div class="preloader">
	    <div class="spinner">
	        <div class="pre-bounce1"></div>
	        <div class="pre-bounce2"></div>
	    </div>
	</div>
	
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>

	
	<header id="masthead" class="site-header" role="banner">
		<div class="header-wrap">
            <div class="container">
                <div class="row">
				<div class="col-md-4 col-sm-8 col-xs-12">
		        					<h1 class="site-title"><a href="http://localhost/wordpress/" rel="home"></a></h1>
					<h2 class="site-description">Swatis Art</h2>	        
		        				</div>
				<div class="col-md-8 col-sm-4 col-xs-12">
					<div class="btn-menu"></div>
					<nav id="mainnav" class="mainnav" role="navigation">
						<div class="menu-english-menu-container"><ul id="menu-english-menu" class="menu"><li id="menu-item-121" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-121"><a href="http://localhost/wordpress">Home</a></li>
<li id="menu-item-116" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-116"><a href="http://localhost/wordpress/?page_id=33">Shop</a></li>
<li id="menu-item-115" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-115"><a href="http://localhost/wordpress/?page_id=34">Cart</a></li>
<li id="menu-item-731" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-731"><a href="http://localhost/wordpress/?page_id=32">Wishlist</a></li>
<li id="menu-item-117" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117"><a href="http://localhost/wordpress/?page_id=36">My account</a></li>
<li id="menu-item-118" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-118"><a href="http://localhost/wordpress/?page_id=35">Checkout</a></li>
</ul></div>					</nav><!-- #site-navigation -->
				</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	
	<div class="sydney-hero-area">
				<div class="header-image">
			<div class="overlay"></div>			<img class="header-inner" src="http://localhost/wordpress/wp-content/uploads/2018/03/30a3231a84e472a561ba69480a5f5073406c39d8-2.jpg" width="1296" alt="" title="">
		</div>
		
			</div>

	
	<div id="content" class="page-wrap">
		<div class="container content-wrapper">
			<div class="row">	
	<div id="primary" class="content-area col-md-9">
		<main id="main" class="post-wrap" role="main">

			
				
<article id="post-1099" class="post-1099 page type-page status-publish hentry">
	<header class="entry-header">
		<h1 class="title-post entry-title">Festivals</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
		<div class="woocommerce columns-4"><ul class="products columns-4">
<li class="product-category product first">
	<a href="http://localhost/wordpress/?product_cat=bouquets"><img src="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-2015-03-31-21.54.21-1-300x300.jpg" alt="Bouquets" width="300" height="300" srcset="http://localhost/wordpress/wp-content/uploads/2018/02/cropped-2015-03-31-21.54.21-1-300x300.jpg 300w, http://localhost/wordpress/wp-content/uploads/2018/02/cropped-2015-03-31-21.54.21-1-150x150.jpg 150w, http://localhost/wordpress/wp-content/uploads/2018/02/cropped-2015-03-31-21.54.21-1-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" />		<h2 class="woocommerce-loop-category__title">
			Bouquets <mark class="count">(15)</mark>		</h2>
		</a></li>
<li class="product-category product">
	<a href="http://localhost/wordpress/?product_cat=christmas"><img src="http://localhost/wordpress/wp-content/uploads/2018/03/christmas2-300x300.jpeg" alt="Christmas" width="300" height="300" srcset="http://localhost/wordpress/wp-content/uploads/2018/03/christmas2-300x300.jpeg 300w, http://localhost/wordpress/wp-content/uploads/2018/03/christmas2-150x150.jpeg 150w, http://localhost/wordpress/wp-content/uploads/2018/03/christmas2-45x45.jpeg 45w, http://localhost/wordpress/wp-content/uploads/2018/03/christmas2-100x100.jpeg 100w" sizes="(max-width: 300px) 100vw, 300px" />		<h2 class="woocommerce-loop-category__title">
			Christmas <mark class="count">(134)</mark>		</h2>
		</a></li>
<li class="product-category product">
	<a href="http://localhost/wordpress/?product_cat=diwali"><img src="http://localhost/wordpress/wp-content/uploads/2018/03/750bf7f891dd13353d6db9328b884706aa64abba-1-300x300.jpg" alt="Diwali" width="300" height="300" srcset="http://localhost/wordpress/wp-content/uploads/2018/03/750bf7f891dd13353d6db9328b884706aa64abba-1-300x300.jpg 300w, http://localhost/wordpress/wp-content/uploads/2018/03/750bf7f891dd13353d6db9328b884706aa64abba-1-150x150.jpg 150w, http://localhost/wordpress/wp-content/uploads/2018/03/750bf7f891dd13353d6db9328b884706aa64abba-1-45x45.jpg 45w, http://localhost/wordpress/wp-content/uploads/2018/03/750bf7f891dd13353d6db9328b884706aa64abba-1-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" />		<h2 class="woocommerce-loop-category__title">
			Diwali <mark class="count">(54)</mark>		</h2>
		</a></li>
<li class="product-category product last">
	<a href="http://localhost/wordpress/?product_cat=diyas"><img src="http://localhost/wordpress/wp-content/uploads/2018/03/IMG-20160716-WA0005-1-300x300.jpg" alt="Diyas" width="300" height="300" srcset="http://localhost/wordpress/wp-content/uploads/2018/03/IMG-20160716-WA0005-1-300x300.jpg 300w, http://localhost/wordpress/wp-content/uploads/2018/03/IMG-20160716-WA0005-1-150x150.jpg 150w, http://localhost/wordpress/wp-content/uploads/2018/03/IMG-20160716-WA0005-1-45x45.jpg 45w, http://localhost/wordpress/wp-content/uploads/2018/03/IMG-20160716-WA0005-1-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px" />		<h2 class="woocommerce-loop-category__title">
			Diyas <mark class="count">(37)</mark>		</h2>
		</a></li>
</ul>
</div>
			</div><!-- .entry-content -->

	<footer class="entry-footer">
			</footer><!-- .entry-footer -->
</article><!-- #post-## -->

				
			
		</main><!-- #main -->
	</div><!-- #primary -->


<div id="secondary" class="widget-area col-md-3" role="complementary">
	<aside id="gtranslate-2" class="widget widget_gtranslate"><h3 class="widget-title">Select your language :)</h3><!-- GTranslate: https://gtranslate.io/ -->
<style type="text/css">
.switcher {font-family:Arial;font-size:10pt;text-align:left;cursor:pointer;overflow:hidden;width:163px;line-height:17px;}
.switcher a {text-decoration:none;display:block;font-size:10pt;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;}
.switcher a img {vertical-align:middle;display:inline;border:0;padding:0;margin:0;opacity:0.8;}
.switcher a:hover img {opacity:1;}
.switcher .selected {background:#FFFFFF url(//localhost/wordpress/wp-content/plugins/gtranslate/switcher.png) repeat-x;position:relative;z-index:9999;}
.switcher .selected a {border:1px solid #CCCCCC;background:url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_down.png) 146px center no-repeat;color:#666666;padding:3px 5px;width:151px;}
.switcher .selected a.open {background-image:url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_up.png)}
.switcher .selected a:hover {background:#F0F0F0 url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_down.png) 146px center no-repeat;}
.switcher .option {position:relative;z-index:9998;border-left:1px solid #CCCCCC;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;background-color:#EEEEEE;display:none;width:161px;max-height:198px;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;overflow-y:auto;overflow-x:hidden;}
.switcher .option a {color:#000;padding:3px 5px;}
.switcher .option a:hover {background:#FFC;}
.switcher .option a.selected {background:#FFC;}
#selected_lang_name {float: none;}
.l_name {float: none !important;margin: 0;}
.switcher .option::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 3px rgba(0,0,0,0.3);border-radius:5px;background-color:#F5F5F5;}
.switcher .option::-webkit-scrollbar {width:5px;}
.switcher .option::-webkit-scrollbar-thumb {border-radius:5px;-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,.3);background-color:#888;}
</style>
<div class="switcher notranslate">
<div class="selected">
<a href="#" onclick="return false;"><img src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/en.png" height="16" width="16" alt="en" /> English</a>
</div>
<div class="option">
<a href="#" onclick="doGTranslate('en|bn');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Bengali" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/bn.png" height="16" width="16" alt="bn" /> Bengali</a><a href="#" onclick="doGTranslate('en|zh-CN');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Chinese (Simplified)" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/zh-CN.png" height="16" width="16" alt="zh-CN" /> Chinese (Simplified)</a><a href="#" onclick="doGTranslate('en|en');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="English" class="nturl selected"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/en.png" height="16" width="16" alt="en" /> English</a><a href="#" onclick="doGTranslate('en|fr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="French" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/fr.png" height="16" width="16" alt="fr" /> French</a><a href="#" onclick="doGTranslate('en|de');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="German" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/de.png" height="16" width="16" alt="de" /> German</a><a href="#" onclick="doGTranslate('en|gu');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Gujarati" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/gu.png" height="16" width="16" alt="gu" /> Gujarati</a><a href="#" onclick="doGTranslate('en|hi');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Hindi" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/hi.png" height="16" width="16" alt="hi" /> Hindi</a><a href="#" onclick="doGTranslate('en|it');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Italian" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/it.png" height="16" width="16" alt="it" /> Italian</a><a href="#" onclick="doGTranslate('en|ja');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Japanese" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/ja.png" height="16" width="16" alt="ja" /> Japanese</a><a href="#" onclick="doGTranslate('en|kn');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Kannada" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/kn.png" height="16" width="16" alt="kn" /> Kannada</a><a href="#" onclick="doGTranslate('en|ml');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Malayalam" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/ml.png" height="16" width="16" alt="ml" /> Malayalam</a><a href="#" onclick="doGTranslate('en|mr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Marathi" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/mr.png" height="16" width="16" alt="mr" /> Marathi</a><a href="#" onclick="doGTranslate('en|es');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Spanish" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/es.png" height="16" width="16" alt="es" /> Spanish</a></div>
</div>
<script type="text/javascript">
jQuery('.switcher .selected').click(function() {jQuery('.switcher .option a img').each(function() {if(!jQuery(this)[0].hasAttribute('src'))jQuery(this).attr('src', jQuery(this).attr('data-gt-lazy-src'))});if(!(jQuery('.switcher .option').is(':visible'))) {jQuery('.switcher .option').stop(true,true).delay(100).slideDown(500);jQuery('.switcher .selected a').toggleClass('open')}});
jQuery('.switcher .option').bind('mousewheel', function(e) {var options = jQuery('.switcher .option');if(options.is(':visible'))options.scrollTop(options.scrollTop() - e.originalEvent.wheelDelta);return false;});
jQuery('body').not('.switcher').click(function(e) {if(jQuery('.switcher .option').is(':visible') && e.target != jQuery('.switcher .option').get(0)) {jQuery('.switcher .option').stop(true,true).delay(100).slideUp(500);jQuery('.switcher .selected a').toggleClass('open')}});
</script>
<style type="text/css">
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
.goog-text-highlight {background-color:transparent !important;box-shadow:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}
</style>

<div id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
function GTranslateGetCurrentLang() {var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');return keyValue ? keyValue[2].split('/')[2] : null;}
function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}
function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];if(GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0])return;var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(/goog-te-combo/.test(sel[i].className)){teCombo=sel[i];break;}if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
if(GTranslateGetCurrentLang() != null)jQuery(document).ready(function() {var lang_html = jQuery('div.switcher div.option').find('img[alt="'+GTranslateGetCurrentLang()+'"]').parent().html();if(typeof lang_html != 'undefined')jQuery('div.switcher div.selected a').html(lang_html.replace('data-gt-lazy-', ''));});
</script>
<script>jQuery(document).ready(function() {var allowed_languages = ["bn","zh-CN","en","fr","de","gu","hi","it","ja","kn","ml","mr","es"];var accept_language = navigator.language.toLowerCase() || navigator.userLanguage.toLowerCase();switch(accept_language) {case 'zh-cn': var preferred_language = 'zh-CN'; break;case 'zh': var preferred_language = 'zh-CN'; break;case 'zh-tw': var preferred_language = 'zh-TW'; break;case 'zh-hk': var preferred_language = 'zh-TW'; break;default: var preferred_language = accept_language.substr(0, 2); break;}if(preferred_language != 'en' && GTranslateGetCurrentLang() == null && document.cookie.match('gt_auto_switch') == null && allowed_languages.indexOf(preferred_language) >= 0){doGTranslate('en|'+preferred_language);document.cookie = 'gt_auto_switch=1; expires=Thu, 05 Dec 2030 08:08:08 UTC; path=/;';var lang_html = jQuery('div.switcher div.option').find('img[alt="'+preferred_language+'"]').parent().html();if(typeof lang_html != 'undefined')jQuery('div.switcher div.selected a').html(lang_html.replace('data-gt-lazy-', ''));}});</script></aside><aside id="aws_widget-5" class="widget widget_aws_widget"><h3 class="widget-title">Search  Here&#8230;</h3><div class="aws-container" data-url="http://localhost/wordpress/wp-admin/admin-ajax.php" data-siteurl="http://localhost/wordpress" data-show-loader="true" data-show-page="false" data-use-analytics="false" data-min-chars="1" ><form class="aws-search-form" action="http://localhost/wordpress" method="get" role="search" ><input  type="text" name="s" value="" class="aws-search-field" placeholder="Search" autocomplete="off" /><input type="hidden" name="post_type" value="product"><input type="hidden" name="type_aws" value="true"><div class="aws-search-clear"><span aria-label="Clear Search">×</span></div></form></div></aside><aside id="woocommerce_product_categories-5" class="widget woocommerce widget_product_categories"><h3 class="widget-title">Product categories</h3><ul class="product-categories"><li class="cat-item cat-item-383 cat-parent"><a href="http://localhost/wordpress/?product_cat=christmas">Christmas</a> <span class="count">(134)</span><ul class='children'>
<li class="cat-item cat-item-53"><a href="http://localhost/wordpress/?product_cat=bouquets">Bouquets</a> <span class="count">(15)</span></li>
<li class="cat-item cat-item-66"><a href="http://localhost/wordpress/?product_cat=containers">Containers</a> <span class="count">(19)</span></li>
<li class="cat-item cat-item-57"><a href="http://localhost/wordpress/?product_cat=covers">Covers</a> <span class="count">(2)</span></li>
<li class="cat-item cat-item-63"><a href="http://localhost/wordpress/?product_cat=decorations">Decorations</a> <span class="count">(53)</span></li>
<li class="cat-item cat-item-59"><a href="http://localhost/wordpress/?product_cat=flowers">Flowers</a> <span class="count">(6)</span></li>
<li class="cat-item cat-item-42"><a href="http://localhost/wordpress/?product_cat=gift_items">Gift Items</a> <span class="count">(133)</span></li>
<li class="cat-item cat-item-71"><a href="http://localhost/wordpress/?product_cat=hangings">Hangings</a> <span class="count">(2)</span></li>
</ul>
</li>
<li class="cat-item cat-item-60"><a href="http://localhost/wordpress/?product_cat=coasters">Coasters</a> <span class="count">(8)</span></li>
<li class="cat-item cat-item-62"><a href="http://localhost/wordpress/?product_cat=dinner_set">Dinner Set</a> <span class="count">(12)</span></li>
<li class="cat-item cat-item-382 cat-parent"><a href="http://localhost/wordpress/?product_cat=diwali">Diwali</a> <span class="count">(54)</span><ul class='children'>
<li class="cat-item cat-item-52 cat-parent"><a href="http://localhost/wordpress/?product_cat=diyas">Diyas</a> <span class="count">(37)</span>	<ul class='children'>
<li class="cat-item cat-item-91"><a href="http://localhost/wordpress/?product_cat=kundan_work">Diyas with kundan work</a> <span class="count">(21)</span></li>
<li class="cat-item cat-item-98"><a href="http://localhost/wordpress/?product_cat=floating_diyas">Floating Diyas</a> <span class="count">(2)</span></li>
<li class="cat-item cat-item-89"><a href="http://localhost/wordpress/?product_cat=glass_diya">Glass Diyas</a> <span class="count">(3)</span></li>
<li class="cat-item cat-item-96"><a href="http://localhost/wordpress/?product_cat=led_diyas">LED diyas</a> <span class="count">(9)</span></li>
<li class="cat-item cat-item-90"><a href="http://localhost/wordpress/?product_cat=mirror_diyas">Mirror Diyas</a> <span class="count">(12)</span></li>
	</ul>
</li>
<li class="cat-item cat-item-50"><a href="http://localhost/wordpress/?product_cat=frames">Frames</a> <span class="count">(15)</span></li>
<li class="cat-item cat-item-58"><a href="http://localhost/wordpress/?product_cat=lamps">Lamps</a> <span class="count">(2)</span></li>
</ul>
</li>
<li class="cat-item cat-item-64"><a href="http://localhost/wordpress/?product_cat=key_stand">Key Stand</a> <span class="count">(9)</span></li>
<li class="cat-item cat-item-61"><a href="http://localhost/wordpress/?product_cat=mats">Mats</a> <span class="count">(3)</span></li>
<li class="cat-item cat-item-15"><a href="http://localhost/wordpress/?product_cat=uncategorized">Uncategorized</a> <span class="count">(2)</span></li>
</ul></aside><aside id="woocommerce_product_tag_cloud-5" class="widget woocommerce widget_product_tag_cloud"><h3 class="widget-title">Product tags</h3><div class="tagcloud"><a href="http://localhost/wordpress/?product_tag=blue" class="tag-cloud-link tag-link-75 tag-link-position-1" style="font-size: 15.933333333333pt;" aria-label="Blue (20 products)">Blue</a>
<a href="http://localhost/wordpress/?product_tag=bouquet" class="tag-cloud-link tag-link-118 tag-link-position-2" style="font-size: 15pt;" aria-label="Bouquet (15 products)">Bouquet</a>
<a href="http://localhost/wordpress/?product_tag=brown" class="tag-cloud-link tag-link-79 tag-link-position-3" style="font-size: 11.733333333333pt;" aria-label="Brown (5 products)">Brown</a>
<a href="http://localhost/wordpress/?product_tag=circle" class="tag-cloud-link tag-link-95 tag-link-position-4" style="font-size: 15.777777777778pt;" aria-label="Circle (19 products)">Circle</a>
<a href="http://localhost/wordpress/?product_tag=coaster" class="tag-cloud-link tag-link-19 tag-link-position-5" style="font-size: 13.055555555556pt;" aria-label="Coaster (8 products)">Coaster</a>
<a href="http://localhost/wordpress/?product_tag=container" class="tag-cloud-link tag-link-111 tag-link-position-6" style="font-size: 16.088888888889pt;" aria-label="Container (21 products)">Container</a>
<a href="http://localhost/wordpress/?product_tag=cover" class="tag-cloud-link tag-link-83 tag-link-position-7" style="font-size: 9.4pt;" aria-label="Cover (2 products)">Cover</a>
<a href="http://localhost/wordpress/?product_tag=dinner-set" class="tag-cloud-link tag-link-116 tag-link-position-8" style="font-size: 12.277777777778pt;" aria-label="Dinner Set (6 products)">Dinner Set</a>
<a href="http://localhost/wordpress/?product_tag=diwali" class="tag-cloud-link tag-link-68 tag-link-position-9" style="font-size: 18.422222222222pt;" aria-label="Diwali (43 products)">Diwali</a>
<a href="http://localhost/wordpress/?product_tag=diya" class="tag-cloud-link tag-link-67 tag-link-position-10" style="font-size: 18.266666666667pt;" aria-label="Diya (41 products)">Diya</a>
<a href="http://localhost/wordpress/?product_tag=floating" class="tag-cloud-link tag-link-100 tag-link-position-11" style="font-size: 9.4pt;" aria-label="Floating (2 products)">Floating</a>
<a href="http://localhost/wordpress/?product_tag=flower" class="tag-cloud-link tag-link-47 tag-link-position-12" style="font-size: 20.133333333333pt;" aria-label="Flower (71 products)">Flower</a>
<a href="http://localhost/wordpress/?product_tag=frame" class="tag-cloud-link tag-link-17 tag-link-position-13" style="font-size: 15.622222222222pt;" aria-label="Frame (18 products)">Frame</a>
<a href="http://localhost/wordpress/?product_tag=gift" class="tag-cloud-link tag-link-51 tag-link-position-14" style="font-size: 22pt;" aria-label="Gift (126 products)">Gift</a>
<a href="http://localhost/wordpress/?product_tag=glass" class="tag-cloud-link tag-link-88 tag-link-position-15" style="font-size: 15pt;" aria-label="Glass (15 products)">Glass</a>
<a href="http://localhost/wordpress/?product_tag=golden" class="tag-cloud-link tag-link-69 tag-link-position-16" style="font-size: 16.555555555556pt;" aria-label="Golden (24 products)">Golden</a>
<a href="http://localhost/wordpress/?product_tag=green" class="tag-cloud-link tag-link-46 tag-link-position-17" style="font-size: 17.022222222222pt;" aria-label="Green (28 products)">Green</a>
<a href="http://localhost/wordpress/?product_tag=hanging" class="tag-cloud-link tag-link-72 tag-link-position-18" style="font-size: 9.4pt;" aria-label="Hanging (2 products)">Hanging</a>
<a href="http://localhost/wordpress/?product_tag=horizontal" class="tag-cloud-link tag-link-77 tag-link-position-19" style="font-size: 10.333333333333pt;" aria-label="Horizontal (3 products)">Horizontal</a>
<a href="http://localhost/wordpress/?product_tag=key-stand" class="tag-cloud-link tag-link-18 tag-link-position-20" style="font-size: 13.444444444444pt;" aria-label="Key Stand (9 products)">Key Stand</a>
<a href="http://localhost/wordpress/?product_tag=kundan" class="tag-cloud-link tag-link-93 tag-link-position-21" style="font-size: 16.788888888889pt;" aria-label="Kundan (26 products)">Kundan</a>
<a href="http://localhost/wordpress/?product_tag=lantern" class="tag-cloud-link tag-link-81 tag-link-position-22" style="font-size: 9.4pt;" aria-label="Lantern (2 products)">Lantern</a>
<a href="http://localhost/wordpress/?product_tag=leaf" class="tag-cloud-link tag-link-76 tag-link-position-23" style="font-size: 14.066666666667pt;" aria-label="Leaf (11 products)">Leaf</a>
<a href="http://localhost/wordpress/?product_tag=led" class="tag-cloud-link tag-link-97 tag-link-position-24" style="font-size: 14.3pt;" aria-label="LED (12 products)">LED</a>
<a href="http://localhost/wordpress/?product_tag=mat" class="tag-cloud-link tag-link-115 tag-link-position-25" style="font-size: 11.733333333333pt;" aria-label="Mat (5 products)">Mat</a>
<a href="http://localhost/wordpress/?product_tag=mirror" class="tag-cloud-link tag-link-94 tag-link-position-26" style="font-size: 14.844444444444pt;" aria-label="Mirror (14 products)">Mirror</a>
<a href="http://localhost/wordpress/?product_tag=mor-pankh" class="tag-cloud-link tag-link-106 tag-link-position-27" style="font-size: 8pt;" aria-label="Mor Pankh (1 product)">Mor Pankh</a>
<a href="http://localhost/wordpress/?product_tag=orange" class="tag-cloud-link tag-link-84 tag-link-position-28" style="font-size: 14.844444444444pt;" aria-label="Orange (14 products)">Orange</a>
<a href="http://localhost/wordpress/?product_tag=paper-bag" class="tag-cloud-link tag-link-78 tag-link-position-29" style="font-size: 8pt;" aria-label="Paper Bag (1 product)">Paper Bag</a>
<a href="http://localhost/wordpress/?product_tag=paper-clips" class="tag-cloud-link tag-link-70 tag-link-position-30" style="font-size: 8pt;" aria-label="Paper Clips (1 product)">Paper Clips</a>
<a href="http://localhost/wordpress/?product_tag=paper-weight" class="tag-cloud-link tag-link-82 tag-link-position-31" style="font-size: 8pt;" aria-label="Paper Weight (1 product)">Paper Weight</a>
<a href="http://localhost/wordpress/?product_tag=peach" class="tag-cloud-link tag-link-108 tag-link-position-32" style="font-size: 12.277777777778pt;" aria-label="Peach (6 products)">Peach</a>
<a href="http://localhost/wordpress/?product_tag=pink" class="tag-cloud-link tag-link-73 tag-link-position-33" style="font-size: 18.033333333333pt;" aria-label="Pink (38 products)">Pink</a>
<a href="http://localhost/wordpress/?product_tag=purple" class="tag-cloud-link tag-link-48 tag-link-position-34" style="font-size: 15.233333333333pt;" aria-label="Purple (16 products)">Purple</a>
<a href="http://localhost/wordpress/?product_tag=quilling" class="tag-cloud-link tag-link-85 tag-link-position-35" style="font-size: 8pt;" aria-label="Quilling (1 product)">Quilling</a>
<a href="http://localhost/wordpress/?product_tag=red" class="tag-cloud-link tag-link-74 tag-link-position-36" style="font-size: 18.033333333333pt;" aria-label="Red (38 products)">Red</a>
<a href="http://localhost/wordpress/?product_tag=silver" class="tag-cloud-link tag-link-99 tag-link-position-37" style="font-size: 11.111111111111pt;" aria-label="Silver (4 products)">Silver</a>
<a href="http://localhost/wordpress/?product_tag=single" class="tag-cloud-link tag-link-86 tag-link-position-38" style="font-size: 9.4pt;" aria-label="Single (2 products)">Single</a>
<a href="http://localhost/wordpress/?product_tag=square" class="tag-cloud-link tag-link-112 tag-link-position-39" style="font-size: 10.333333333333pt;" aria-label="Square (3 products)">Square</a>
<a href="http://localhost/wordpress/?product_tag=stone" class="tag-cloud-link tag-link-87 tag-link-position-40" style="font-size: 11.111111111111pt;" aria-label="Stone (4 products)">Stone</a>
<a href="http://localhost/wordpress/?product_tag=table-decoration" class="tag-cloud-link tag-link-102 tag-link-position-41" style="font-size: 12.277777777778pt;" aria-label="Table Decoration (6 products)">Table Decoration</a>
<a href="http://localhost/wordpress/?product_tag=vase" class="tag-cloud-link tag-link-103 tag-link-position-42" style="font-size: 10.333333333333pt;" aria-label="Vase (3 products)">Vase</a>
<a href="http://localhost/wordpress/?product_tag=vertical" class="tag-cloud-link tag-link-49 tag-link-position-43" style="font-size: 12.277777777778pt;" aria-label="Vertical (6 products)">Vertical</a>
<a href="http://localhost/wordpress/?product_tag=white" class="tag-cloud-link tag-link-80 tag-link-position-44" style="font-size: 19.044444444444pt;" aria-label="White (52 products)">White</a>
<a href="http://localhost/wordpress/?product_tag=yellow" class="tag-cloud-link tag-link-92 tag-link-position-45" style="font-size: 14.066666666667pt;" aria-label="Yellow (11 products)">Yellow</a></div></aside></div><!-- #secondary -->
			</div>
		</div>
	</div><!-- #content -->

	
			

	
	<div id="sidebar-footer" class="footer-widgets widget-area" role="complementary">
		<div class="container">
							<div class="sidebar-column col-md-4">
					<aside id="sydney_contact_info-3" class="widget sydney_contact_info_widget"><h3 class="widget-title">GET IN TOUCH</h3><div class="contact-address"><span><i class="fa fa-home"></i></span>&quot;KARUNA&quot;,39/10-11, Raliway Lines, Solapur</div><div class="contact-phone"><span><i class="fa fa-phone"></i></span>9850563163</div><div class="contact-email"><span><i class="fa fa-envelope"></i></span><a href="mailto:&#100;esai&#46;s&#119;&#97;t&#105;&#64;yahoo.&#105;&#110;">&#100;esai&#46;s&#119;&#97;t&#105;&#64;yahoo.&#105;&#110;</a></div></aside>				</div>
				
							<div class="sidebar-column col-md-4">
					<aside id="sow-social-media-buttons-3" class="widget widget_sow-social-media-buttons"><div class="so-widget-sow-social-media-buttons so-widget-sow-social-media-buttons-atom-4b7a7eb6ba77">
<div class="social-media-button-container">
	
		<a class="ow-button-hover sow-social-media-button-facebook sow-social-media-button" title=" on Facebook" aria-label=" on Facebook" target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/dswati8/" >
			<span>
								<span class="sow-icon-fontawesome" data-sow-icon="&#xf09a;" ></span>							</span>
		</a>
	
		<a class="ow-button-hover sow-social-media-button-google-plus sow-social-media-button" title=" on Google Plus" aria-label=" on Google Plus" target="_blank" rel="noopener noreferrer" href="https://plus.google.com/" >
			<span>
								<span class="sow-icon-fontawesome" data-sow-icon="&#xf0d5;" ></span>							</span>
		</a>
	
		<a class="ow-button-hover sow-social-media-button-linkedin sow-social-media-button" title=" on Linkedin" aria-label=" on Linkedin" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/" >
			<span>
								<span class="sow-icon-fontawesome" data-sow-icon="&#xf0e1;" ></span>							</span>
		</a>
	
		<a class="ow-button-hover sow-social-media-button-pinterest sow-social-media-button" title=" on Pinterest" aria-label=" on Pinterest" target="_blank" rel="noopener noreferrer" href="https://www.pinterest.com/" >
			<span>
								<span class="sow-icon-fontawesome" data-sow-icon="&#xf0d2;" ></span>							</span>
		</a>
	</div>
</div></aside><aside id="gtranslate-3" class="widget widget_gtranslate"><h3 class="widget-title">Select your lanugage :)</h3><!-- GTranslate: https://gtranslate.io/ -->
<style type="text/css">
.switcher {font-family:Arial;font-size:10pt;text-align:left;cursor:pointer;overflow:hidden;width:163px;line-height:17px;}
.switcher a {text-decoration:none;display:block;font-size:10pt;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;}
.switcher a img {vertical-align:middle;display:inline;border:0;padding:0;margin:0;opacity:0.8;}
.switcher a:hover img {opacity:1;}
.switcher .selected {background:#FFFFFF url(//localhost/wordpress/wp-content/plugins/gtranslate/switcher.png) repeat-x;position:relative;z-index:9999;}
.switcher .selected a {border:1px solid #CCCCCC;background:url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_down.png) 146px center no-repeat;color:#666666;padding:3px 5px;width:151px;}
.switcher .selected a.open {background-image:url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_up.png)}
.switcher .selected a:hover {background:#F0F0F0 url(//localhost/wordpress/wp-content/plugins/gtranslate/arrow_down.png) 146px center no-repeat;}
.switcher .option {position:relative;z-index:9998;border-left:1px solid #CCCCCC;border-right:1px solid #CCCCCC;border-bottom:1px solid #CCCCCC;background-color:#EEEEEE;display:none;width:161px;max-height:198px;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;overflow-y:auto;overflow-x:hidden;}
.switcher .option a {color:#000;padding:3px 5px;}
.switcher .option a:hover {background:#FFC;}
.switcher .option a.selected {background:#FFC;}
#selected_lang_name {float: none;}
.l_name {float: none !important;margin: 0;}
.switcher .option::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 3px rgba(0,0,0,0.3);border-radius:5px;background-color:#F5F5F5;}
.switcher .option::-webkit-scrollbar {width:5px;}
.switcher .option::-webkit-scrollbar-thumb {border-radius:5px;-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,.3);background-color:#888;}
</style>
<div class="switcher notranslate">
<div class="selected">
<a href="#" onclick="return false;"><img src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/en.png" height="16" width="16" alt="en" /> English</a>
</div>
<div class="option">
<a href="#" onclick="doGTranslate('en|bn');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Bengali" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/bn.png" height="16" width="16" alt="bn" /> Bengali</a><a href="#" onclick="doGTranslate('en|zh-CN');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Chinese (Simplified)" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/zh-CN.png" height="16" width="16" alt="zh-CN" /> Chinese (Simplified)</a><a href="#" onclick="doGTranslate('en|en');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="English" class="nturl selected"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/en.png" height="16" width="16" alt="en" /> English</a><a href="#" onclick="doGTranslate('en|fr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="French" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/fr.png" height="16" width="16" alt="fr" /> French</a><a href="#" onclick="doGTranslate('en|de');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="German" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/de.png" height="16" width="16" alt="de" /> German</a><a href="#" onclick="doGTranslate('en|gu');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Gujarati" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/gu.png" height="16" width="16" alt="gu" /> Gujarati</a><a href="#" onclick="doGTranslate('en|hi');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Hindi" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/hi.png" height="16" width="16" alt="hi" /> Hindi</a><a href="#" onclick="doGTranslate('en|it');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Italian" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/it.png" height="16" width="16" alt="it" /> Italian</a><a href="#" onclick="doGTranslate('en|ja');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Japanese" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/ja.png" height="16" width="16" alt="ja" /> Japanese</a><a href="#" onclick="doGTranslate('en|kn');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Kannada" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/kn.png" height="16" width="16" alt="kn" /> Kannada</a><a href="#" onclick="doGTranslate('en|ml');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Malayalam" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/ml.png" height="16" width="16" alt="ml" /> Malayalam</a><a href="#" onclick="doGTranslate('en|mr');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Marathi" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/mr.png" height="16" width="16" alt="mr" /> Marathi</a><a href="#" onclick="doGTranslate('en|es');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Spanish" class="nturl"><img data-gt-lazy-src="//localhost/wordpress/wp-content/plugins/gtranslate/flags/16/es.png" height="16" width="16" alt="es" /> Spanish</a></div>
</div>
<script type="text/javascript">
jQuery('.switcher .selected').click(function() {jQuery('.switcher .option a img').each(function() {if(!jQuery(this)[0].hasAttribute('src'))jQuery(this).attr('src', jQuery(this).attr('data-gt-lazy-src'))});if(!(jQuery('.switcher .option').is(':visible'))) {jQuery('.switcher .option').stop(true,true).delay(100).slideDown(500);jQuery('.switcher .selected a').toggleClass('open')}});
jQuery('.switcher .option').bind('mousewheel', function(e) {var options = jQuery('.switcher .option');if(options.is(':visible'))options.scrollTop(options.scrollTop() - e.originalEvent.wheelDelta);return false;});
jQuery('body').not('.switcher').click(function(e) {if(jQuery('.switcher .option').is(':visible') && e.target != jQuery('.switcher .option').get(0)) {jQuery('.switcher .option').stop(true,true).delay(100).slideUp(500);jQuery('.switcher .selected a').toggleClass('open')}});
</script>
<style type="text/css">
#goog-gt-tt {display:none !important;}
.goog-te-banner-frame {display:none !important;}
.goog-te-menu-value:hover {text-decoration:none !important;}
.goog-text-highlight {background-color:transparent !important;box-shadow:none !important;}
body {top:0 !important;}
#google_translate_element2 {display:none!important;}
</style>

<div id="google_translate_element2"></div>
<script type="text/javascript">
function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'en',autoDisplay: false}, 'google_translate_element2');}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>


<script type="text/javascript">
function GTranslateGetCurrentLang() {var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');return keyValue ? keyValue[2].split('/')[2] : null;}
function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}
function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];if(GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0])return;var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(/goog-te-combo/.test(sel[i].className)){teCombo=sel[i];break;}if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
if(GTranslateGetCurrentLang() != null)jQuery(document).ready(function() {var lang_html = jQuery('div.switcher div.option').find('img[alt="'+GTranslateGetCurrentLang()+'"]').parent().html();if(typeof lang_html != 'undefined')jQuery('div.switcher div.selected a').html(lang_html.replace('data-gt-lazy-', ''));});
</script>
<script>jQuery(document).ready(function() {var allowed_languages = ["bn","zh-CN","en","fr","de","gu","hi","it","ja","kn","ml","mr","es"];var accept_language = navigator.language.toLowerCase() || navigator.userLanguage.toLowerCase();switch(accept_language) {case 'zh-cn': var preferred_language = 'zh-CN'; break;case 'zh': var preferred_language = 'zh-CN'; break;case 'zh-tw': var preferred_language = 'zh-TW'; break;case 'zh-hk': var preferred_language = 'zh-TW'; break;default: var preferred_language = accept_language.substr(0, 2); break;}if(preferred_language != 'en' && GTranslateGetCurrentLang() == null && document.cookie.match('gt_auto_switch') == null && allowed_languages.indexOf(preferred_language) >= 0){doGTranslate('en|'+preferred_language);document.cookie = 'gt_auto_switch=1; expires=Thu, 05 Dec 2030 08:08:08 UTC; path=/;';var lang_html = jQuery('div.switcher div.option').find('img[alt="'+preferred_language+'"]').parent().html();if(typeof lang_html != 'undefined')jQuery('div.switcher div.selected a').html(lang_html.replace('data-gt-lazy-', ''));}});</script></aside>				</div>
				
							<div class="sidebar-column col-md-4">
					<aside id="youtubesubs_widget-2" class="widget widget_youtubesubs_widget"><h3 class="widget-title">Subscribe to YouTube</h3><div class="g-ytsubscribe" data-channel="BBCNews" data-layout="default" data-count="default"></div></aside>				</div>
				
				
		</div>	
	</div>	
    <a class="go-top"><i class="fa fa-angle-up"></i></a>
		
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info container">
			<a href="http://wordpress.org/">Proudly powered by WordPress</a>
			<span class="sep"> | </span>
			Theme: <a href="https://athemes.com/theme/sydney" rel="designer">Sydney</a> by aThemes.		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	
</div><!-- #page -->

<div id="pum-756" class="pum pum-overlay pum-theme-751 pum-theme-lightbox popmake-overlay click_open" data-popmake="{&quot;id&quot;:756,&quot;slug&quot;:&quot;subscribe-to-swatis-art&quot;,&quot;theme_id&quot;:751,&quot;cookies&quot;:[],&quot;triggers&quot;:[{&quot;type&quot;:&quot;click_open&quot;,&quot;settings&quot;:{&quot;cookie_name&quot;:null,&quot;extra_selectors&quot;:&quot; .ow-icon-placement-right &gt; span, a[href=\&quot;exact_url\&quot;]&quot;}}],&quot;mobile_disabled&quot;:null,&quot;tablet_disabled&quot;:null,&quot;meta&quot;:{&quot;display&quot;:{&quot;size&quot;:&quot;medium&quot;,&quot;responsive_min_width&quot;:&quot;0%&quot;,&quot;responsive_max_width&quot;:&quot;500px&quot;,&quot;custom_width&quot;:&quot;640px&quot;,&quot;custom_height&quot;:&quot;380px&quot;,&quot;animation_type&quot;:&quot;fade&quot;,&quot;animation_speed&quot;:&quot;240&quot;,&quot;animation_origin&quot;:&quot;center top&quot;,&quot;position_bottom&quot;:&quot;0&quot;,&quot;location&quot;:&quot;left top&quot;,&quot;position_right&quot;:&quot;0&quot;,&quot;position_top&quot;:&quot;193&quot;,&quot;position_left&quot;:&quot;452&quot;,&quot;overlay_zindex&quot;:false,&quot;zindex&quot;:&quot;1999999999&quot;,&quot;responsive_min_width_unit&quot;:false,&quot;responsive_max_width_unit&quot;:false,&quot;custom_width_unit&quot;:false,&quot;custom_height_unit&quot;:false,&quot;stackable&quot;:false,&quot;overlay_disabled&quot;:false,&quot;scrollable_content&quot;:false,&quot;disable_reposition&quot;:false,&quot;custom_height_auto&quot;:false,&quot;position_from_trigger&quot;:false,&quot;position_fixed&quot;:false},&quot;close&quot;:{&quot;text&quot;:&quot;&quot;,&quot;button_delay&quot;:&quot;200&quot;,&quot;esc_press&quot;:&quot;1&quot;,&quot;f4_press&quot;:&quot;1&quot;,&quot;overlay_click&quot;:false},&quot;click_open&quot;:[]}}" role="dialog" aria-hidden="true" aria-labelledby="pum_popup_title_756">

	<div id="popmake-756" class="pum-container popmake theme-751 pum-responsive pum-responsive-medium responsive size-medium">

				

				            <div id="pum_popup_title_756" class="pum-title popmake-title">
				SWATI'S ART			</div>
		

		

				<div class="pum-content popmake-content">
			<p><b><i><center>Kacchi Dhoop Mai&#8217;s trendy prodcuts !!</center></i></b><br />
<div role="form" class="wpcf7" id="wpcf7-f38-o2" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/wordpress/?page_id=1099#wpcf7-f38-o2" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="38" />
<input type="hidden" name="_wpcf7_version" value="5.0.1" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f38-o2" />
<input type="hidden" name="_wpcf7_container_post" value="0" />
</div>
<p><label> Your Email (required)<br />
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" /></span> </label></p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" /></p>
<input type='hidden' class='wpcf7-pum' value='{"closepopup":false,"closedelay":0,"openpopup":true,"openpopup_id":756}' /><div class="wpcf7-response-output wpcf7-display-none"></div></form></div></p>
		</div>


				

				            <button type="button" class="pum-close popmake-close" aria-label="Close">
			×CLOSE            </button>
		
	</div>

</div>
<link rel='stylesheet' id='sow-social-media-buttons-atom-4b7a7eb6ba77-css'  href='http://localhost/wordpress/wp-content/uploads/siteorigin-widgets/sow-social-media-buttons-atom-4b7a7eb6ba77.css?ver=4.9.5' type='text/css' media='all' />
<link rel='stylesheet' id='siteorigin-widget-icon-font-fontawesome-css'  href='http://localhost/wordpress/wp-content/plugins/so-widgets-bundle/icons/fontawesome/style.css?ver=4.9.5' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/wordpress\/index.php?rest_route=\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
var wpcf7 = {"apiSettings":{"root":"http:\/\/localhost\/wordpress\/index.php?rest_route=\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_add_to_cart_params = {"ajax_url":"\/wordpress\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wordpress\/?wc-ajax=%%endpoint%%","i18n_view_cart":"View cart","cart_url":"http:\/\/localhost\/wordpress\/?page_id=34","is_cart":"","cart_redirect_after_add":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=3.3.5'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var woocommerce_params = {"ajax_url":"\/wordpress\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wordpress\/?wc-ajax=%%endpoint%%"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=3.3.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wc_cart_fragments_params = {"ajax_url":"\/wordpress\/wp-admin\/admin-ajax.php","wc_ajax_url":"\/wordpress\/?wc-ajax=%%endpoint%%","cart_hash_key":"wc_cart_hash_224cacb54a1b7e066f5693afaf0d2b45","fragment_name":"wc_fragments_224cacb54a1b7e066f5693afaf0d2b45"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=3.3.5'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var aws_vars = {"sale":"Sale!","noresults":"Nothing found"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/advanced-woo-search/assets/js/common.js?ver=1.39'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var yith_wcwl_l10n = {"ajax_url":"\/wordpress\/wp-admin\/admin-ajax.php","redirect_to_cart":"no","multi_wishlist":"","hide_add_button":"1","is_user_logged_in":"","ajax_loader_url":"http:\/\/localhost\/wordpress\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif","remove_from_wishlist_after_add_to_cart":"yes","labels":{"cookie_disabled":"We are sorry, but this feature is available only if cookies are enabled on your browser.","added_to_cart_message":"<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"},"actions":{"add_to_wishlist_action":"add_to_wishlist","remove_from_wishlist_action":"remove_from_wishlist","move_to_another_wishlist_action":"move_to_another_wishlsit","reload_wishlist_and_adding_elem_action":"reload_wishlist_and_adding_elem"}};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.js?ver=2.2.1'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/themes/sydney/js/scripts.js?ver=4.9.5'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/themes/sydney/js/main.min.js?ver=20180213'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-content/themes/sydney/js/skip-link-focus-fix.js?ver=20130115'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pum_vars = {"version":"1.7.15","ajaxurl":"http:\/\/localhost\/wordpress\/wp-admin\/admin-ajax.php","restapi":"http:\/\/localhost\/wordpress\/index.php?rest_route=\/pum\/v1","rest_nonce":null,"default_theme":"750","debug_mode":"","popups":{"pum-756":{"disable_on_mobile":false,"disable_on_tablet":false,"custom_height_auto":false,"scrollable_content":false,"position_from_trigger":false,"position_fixed":false,"overlay_disabled":false,"stackable":false,"disable_reposition":false,"close_on_overlay_click":false,"close_on_esc_press":true,"close_on_f4_press":true,"triggers":[{"type":"click_open","settings":{"cookie_name":null,"extra_selectors":" .ow-icon-placement-right > span, a[href=\"exact_url\"]"}}],"theme_id":"751","size":"medium","responsive_min_width":"0%","responsive_max_width":"500px","custom_width":"640px","custom_height":"380px","animation_type":"fade","animation_speed":"240","animation_origin":"center top","location":"left top","position_top":"193","position_bottom":"0","position_left":"452","position_right":"0","zindex":"1999999999","close_button_delay":"200","cookies":[],"id":756,"slug":"subscribe-to-swatis-art"}},"disable_tracking":"","home_url":"\/wordpress\/","message_position":"top","core_sub_forms_enabled":"1"};
var ajaxurl = "http:\/\/localhost\/wordpress\/wp-admin\/admin-ajax.php";
var pum_debug_vars = {"debug_mode_enabled":"Popup Maker: Debug Mode Enabled","debug_started_at":"Debug started at:","debug_more_info":"For more information on how to use this information visit https:\/\/docs.wppopupmaker.com\/?utm_medium=js-debug-info&utm_campaign=ContextualHelp&utm_source=browser-console&utm_content=more-info","global_info":"Global Information","localized_vars":"Localized variables","popups_initializing":"Popups Initializing","popups_initialized":"Popups Initialized","single_popup_label":"Popup: #","theme_id":"Theme ID: ","label_method_call":"Method Call:","label_method_args":"Method Arguments:","label_popup_settings":"Settings","label_triggers":"Triggers","label_cookies":"Cookies","label_delay":"Delay:","label_conditions":"Conditions","label_cookie":"Cookie:","label_settings":"Settings:","label_selector":"Selector:","label_mobile_disabled":"Mobile Disabled:","label_tablet_disabled":"Tablet Disabled:","label_event":"Event: %s","triggers":{"click_open":"Click Open","auto_open":"Time Delay \/ Auto Open"},"cookies":{"on_popup_close":"On Popup Close","on_popup_open":"On Popup Open","pum_sub_form_success":"Subscription Form: Successful","pum_sub_form_already_subscribed":"Subscription Form: Already Subscribed","manual":"Manual JavaScript","cf7_form_success":"Contact Form 7 Success"}};
var pum_sub_vars = {"ajaxurl":"http:\/\/localhost\/wordpress\/wp-admin\/admin-ajax.php","message_position":"top"};
/* ]]> */
</script>
<script type='text/javascript' src='//localhost/wordpress/wp-content/uploads/pum/pum-site-scripts.js?defer&#038;generated=1520827228&#038;ver=1.7.15'></script>
<script type='text/javascript' src='http://localhost/wordpress/wp-includes/js/wp-embed.min.js?ver=4.9.5'></script>
<script type="text/javascript">myButton = undefined;</script>
</body>
</html>

<!-- Dynamic page generated in 5.178 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2018-04-15 08:22:57 -->
