<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class PUM {

	const VER = '1.7.15-beta1';

	const DB_VER = 8;

	const API_URL = 'https://wppopupmaker.com/?edd_action=';

}
