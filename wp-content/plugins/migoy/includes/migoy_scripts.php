<?php
	//	Adding scripts

function mig_add_scripts(){
	//Add CSS
	wp_enqueue_style('mig-main-style',plugins_url().'/migoy/css/style.css');
	//Add JS
	wp_enqueue_script('mig-main-style',plugins_url().'/migoy/js/main.js');
	//Add Google Script
	wp_register_script('google','https://apis.google.com/js/platform.js');
	wp_enqueue_script('google');
}
	//Hook to call mig_add_scripts function

add_action('wp_enqueue_scripts','mig_add_scripts');