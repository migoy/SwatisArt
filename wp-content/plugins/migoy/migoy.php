<?php
/*
Plugin Name: Migoy
Plugin URI: 
Description: Checks the number of your youtube subscribers
Version: 1.1.1
Author: MIGOY
Author URI: 
Text Domain: youtube-check
Domain Path: /languages
*/

//	Exit if accessed directly

if(!defined('ABSPATH')){
	exit;
}
//	Loads scripts 
require_once(plugin_dir_path(__FILE__).'/includes/migoy_scripts.php');

//	Loads Class
require_once(plugin_dir_path(__FILE__).'/includes/migoy-class.php');

//	Register Widget to see it
function register_youtubesubs(){
	register_widget('Youtube_Subs_Widget');
}

//	Hook in function
add_action('widgets_init','register_youtubesubs');
